#!/usr/bin/env bash

mkdir /home/vagrant/GP2
cd /home/vagrant/GP2

export CC=/usr/bin/gcc
export CXX=/usr/bin/g++
sudo ln -s /usr/bin/make /usr/bin/gmake
sudo ln -s /usr/bin/make /usr/local/bin/gmake

git clone https://github.com/UoYCS-plasma/GP2.git
cd GP2/Compiler
sudo apt-get install bison flex libperl-dev libgtk2.0-dev pandoc -y
aclocal
autoconf
autoreconf --install
automake --add-missing --foreign
./configure
make -j4
sudo make install
cd ../../

wget https://yorkcs.gitlab.io/cdn/gp2/vagrant/ogdf.v2012.07.zip
unzip -a ogdf.v2012.07.zip
rm ogdf.v2012.07.zip
mkdir OGDF-build
cd OGDF-source
find . -type f -exec sed -i 's/\/local\/sda5\/ish503\/root\/OGDF-source/\/home\/vagrant\/GP2\/OGDF-source/g' {} \;
find . -type f -exec sed -i 's/\/local\/sda5\/ish503\/root\/root\/OGDF-build/\/home\/vagrant\/GP2\/OGDF-build/g' {} \;
cmake -DCMAKE_INSTALL_PREFIX=../OGDF-build
make -j4
make install
cd ../

git clone https://github.com/UoYCS-plasma/GP2-editor.git
cd GP2-editor
sudo apt-get install libboost-dev libqt4-dev -y
sed -i 's/\~\/github\/GP2\/Compiler/\/home\/vagrant\/GP2\/GP2\/Compiler/g' CMakeLists.txt
sed -i 's/\/local\/sda5\/ish503\/root/\/home\/vagrant\/GP2/g' CMakeLists.txt
sed -i 's/SET\(BOOST_INCLUDE_DIR \/home\/vagrant\/GP2\)//g' CMakeLists.txt
sed -i 's/Boost 1.49.0/Boost 1.58.0/g' CMakeLists.txt
cd ../
mkdir GP2-build
cd GP2-build
cmake ../GP2-editor
make -j4
